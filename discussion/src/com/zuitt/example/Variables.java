//A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
    // Packages are divided into two categories:
        // 1. Built-in Packages (Packages from the JAVA API)
        // 2. User-defined Packages (create your own packages)
package com.zuitt.example;
    // Package creation follows the "reverse domain name notation" for the naming convention.
    // The syntax is useful because the ordering of the named components gets reversed, surfacing the logical groupings inherent in the designed structure of DNS.
public class Variables {
        // Naming convention

        // The terminology used for variable names is "identifier".
        // All identifiers should begin with a letter (A to Z or a to z), currency character ($) or an underscore.
        // After the first character, identifiers can have any combination of characters.
        // A keyword cannot be used as an identifier.
        // Most importantly, identifiers are case sensitive.
        // Syntax: dataType identifier

        public static void main(String[] args){
            //variable
            int age;
            char middleName;

            //variable declaration versus initialization
            int x;
            int y = 0;

            // Initialization after declaration
            x = 1;

           /* output to the systems*/

            System.out.println("The value of y is "+ y + " and the value of x is " + x);

            //predefined within the Java programming language which is used for single-valued variables with limited capabilities.


            /*int - whole number values*/

            int wholeNumber = 1000;

            System.out.println(wholeNumber);

            /*long L is added to the end of the Long numbers to be recognized*/
            long worldPopulation = 78636213612767L;

            System.out.println(worldPopulation);

            /*float*/
            /*add f at the end of a float to be recognized*/

            float piFloat = 3.14159265359f;
            System.out.println(piFloat);

            /*double - floating point values*/
            double piDouble = 3.14159265359;
         }
}
